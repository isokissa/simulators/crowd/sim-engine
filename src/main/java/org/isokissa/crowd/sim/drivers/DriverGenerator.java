package org.isokissa.crowd.sim.drivers;

import java.util.function.Supplier;

public interface DriverGenerator extends Supplier<Driver> {}
