package org.isokissa.crowd.sim.drivers;

import org.isokissa.crowd.sim.Environment;

public class EnvironmentAwareDriver extends Driver {
    protected Environment environment;

    public EnvironmentAwareDriver(Environment environment) {
        this.environment = environment;
    }
}
