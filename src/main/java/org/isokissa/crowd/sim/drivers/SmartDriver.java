package org.isokissa.crowd.sim.drivers;

import org.isokissa.crowd.sim.*;
import org.isokissa.crowd.sim.commands.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SmartDriver extends EnvironmentAwareDriver {
    public enum State {
        INITIAL {
            @Override
            List<Command> drive(SmartDriver d, Body body, float seconds) {
                float velocityDifference = d.cruisingVelocity - body.getVelocity();
                if (Math.abs(velocityDifference) > seconds * MAX_ACCELERATION) {
                    return List.of(() -> body.advance(seconds,
                            Math.signum(velocityDifference) * MAX_ACCELERATION,
                            0));
                } else {
                    d.setState(NORMAL);
                    return List.of(
                            () -> body.advance(seconds, velocityDifference / seconds, 0),
                            () -> body.setState(PlaneBody.State.NORMAL));
                }
            }
        },

        NORMAL {
            @Override
            List<Command> drive(SmartDriver d, Body body, float seconds) {
                List<Command> commands = new ArrayList<>();
                Optional<CollisionDetector.CollisionInformation> firstCollision = d.findFirstBodyToCollideWith(body);
                if (firstCollision.isPresent()) {
                    switch (firstCollision.get().direction) {
                        case FROM_BACK:
                            commands.add(() -> body.advance(seconds, 0, SmartDriver.GIVE_WAY));
                            break;
                        case APPROACHING_THE_SLOWER:
                            commands.add(() -> body.advance(seconds, 0, OVERTAKING_STEERING));
                            break;
                        case FROM_LEFT:
                            commands.add(() -> body.advance(seconds, 0, - OVERTAKING_STEERING));
                            break;
                        case FROM_RIGHT:
                            commands.add(() -> body.advance(seconds, 0, - OVERTAKING_STEERING));
                            break;
                        default:
                            commands.add(() -> body.advance(seconds, 0, 0));
                    }
                } else {
                    float targetAngle = Geometry.angleTo(body.getX(), body.getY(), d.destX, d.destY);
                    float angleDifference = Geometry.angleMinDifference(body.getAngle(), targetAngle);
                    float velocityDifference = d.cruisingVelocity - body.getVelocity();
                    if (angleDifference != 0 || Math.abs(velocityDifference) > 0.05f) {
                        final float steering = (Math.abs(angleDifference) > seconds * MAX_STEERING_SPEED) ?
                                Math.signum(angleDifference) * MAX_STEERING_SPEED : angleDifference / seconds;
                        final float acceleration = Math.abs(velocityDifference) > seconds * MAX_ACCELERATION ?
                                Math.signum(velocityDifference) * MAX_ACCELERATION : velocityDifference / seconds;
                        commands.add(() -> body.advance(seconds, acceleration, steering));
                    } else {
                        commands.add(() -> body.advance(seconds, 0, 0));
                    }
                }
                return commands;
            }
        };

        List<Command> drive(SmartDriver d, Body body, float seconds) {
            return List.of();
        }
    }

    public static final float MAX_ACCELERATION = 5f;
    public static final float MAX_STEERING_SPEED = 55;
    public static final float OVERTAKING_STEERING = 10;
    public static final float GIVE_WAY = -5;
    public static final float SCAN_SIZE = 40;
    private float destX;
    private float destY;
    private float cruisingVelocity;
    private State state = State.INITIAL;

    public SmartDriver(Environment environment, float destX, float destY, float cruisingVelocity) {
        super(environment);
        this.destX = destX;
        this.destY = destY;
        this.cruisingVelocity = cruisingVelocity;
    }

    /**
     * Drives the body, based on body's state and whatever state the
     * subclasses of this class define.
     *
     * @param body a body to drive
     * @param seconds
     * @return List of commands for this body, other bodies, and arena.
     */
    @Override
    public List<Command> drive(Body body, float seconds) {
        return state.drive(this, body, seconds);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    private Optional<CollisionDetector.CollisionInformation> findFirstBodyToCollideWith(Body body) {
        List<PlaneBody> bodies = environment.getBodies(
                body.getX() - SCAN_SIZE, body.getX() + SCAN_SIZE,
                body.getY() - SCAN_SIZE, body.getY() + SCAN_SIZE);
        Optional<CollisionDetector.CollisionInformation> nearestColliding = bodies.parallelStream()
                .map(otherBody -> CollisionDetector.calculate(body, otherBody))
                .filter(collInfo -> collInfo.isPresent() && collInfo.get().secondsToCollision < 5f)
                .map(collisionInformation -> collisionInformation.get())
                .min((inf1, inf2) -> (int)(inf2.secondsToCollision - inf1.secondsToCollision));
        return nearestColliding;
    }
}
