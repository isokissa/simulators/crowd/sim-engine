package org.isokissa.crowd.sim.drivers;

import org.isokissa.crowd.sim.Body;
import org.isokissa.crowd.sim.PlaneBody;
import org.isokissa.crowd.sim.commands.Command;

import java.util.ArrayList;
import java.util.List;

public class BlindDriver extends Driver {
    public static final float MAX_ACCELERATION = 5f;
    private float constantVelocity;
    private boolean changedToNormal = false;

    public BlindDriver(float constantVelocity) {
        this.constantVelocity = constantVelocity;
    }

    @Override
    public List<Command> drive(Body body, float seconds) {
        float velocityDelta = constantVelocity - body.getVelocity();
        float acceleration = Math.min(Math.abs(velocityDelta) / seconds, MAX_ACCELERATION)
                             * Math.signum(velocityDelta);
        Command advanceCommand = () -> {
            body.advance(seconds, acceleration, 0);
        };
        List<Command> commands = new ArrayList<>();
        commands.add(advanceCommand);
        if (!changedToNormal && body.getVelocity() >= constantVelocity) {
            Command changeStateToNormalCommand = () -> body.setState(PlaneBody.State.NORMAL);
            changedToNormal = true;
            commands.add(changeStateToNormalCommand);
        }
        return commands;
    }
}
