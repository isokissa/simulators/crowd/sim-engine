package org.isokissa.crowd.sim.drivers;

import org.isokissa.crowd.sim.Body;
import org.isokissa.crowd.sim.commands.Command;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements strategy/state pattern, different ways to drive
 */
public class Driver {
    /**
     * Drives the body, based on body's state and whatever state the
     * subclasses of this class define.
     * @param body a body to drive
     * @param seconds
     * @return List of commands for this body, other bodies, and arena.
     */
    public List<Command> drive(Body body, float seconds) {
        return new ArrayList<>();
    }
}
