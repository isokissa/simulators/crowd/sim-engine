package org.isokissa.crowd.sim;

import org.isokissa.crowd.sim.commands.Command;
import org.isokissa.crowd.sim.drivers.Driver;

import java.util.List;

public class Body implements PlaneBody, BodyControl {
    private State state = State.INVISIBLE;
    private float x;
    private float y;
    private float velocity = 0;
    private float angle = 0;
    private float sinAngle = 0;
    private float cosAngle = 0;
    private Driver driver;

    @Override
    public State getState() {
        return state;
    }

    @Override
    public void setState(State newState) {
        state = newState;
    }

    /**
     * Sets new driver
     *
     * @param driver new Driver
     */
    @Override
    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    /**
     * Creates new body at given location and given initial angle. Assuming that the
     * initial angle will be between -360 and +360. The method will normalize it so that
     * {@code getAngle()} will return always angle in interval [0, 360).
     * @param x
     * @param y
     * @param angle
     */
    public Body(float x, float y, float angle) {
        this.x = x;
        this.y = y;
        this.angle = Geometry.normalizeAngle(angle);
        calculateCachedSinAndCos();
    }

    public Body(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public float getX() {
        return x;
    }

    @Override
    public float getY() {
        return y;
    }

    @Override
    public float getAngle() {
        return angle;
    }

    @Override
    public float getCosAngle() {
        return cosAngle;
    }

    @Override
    public float getSinAngle() {
        return sinAngle;
    }

    @Override
    public float getVelocity() {
        return velocity;
    }

    @Override
    public void advance(float seconds, float acceleration, float steeringSpeed) {
        if (State.COLLIDED == state) {
            velocity = 0;
            return;
        }
        if (0 != steeringSpeed) {
            angle = angle + steeringSpeed * seconds;
            angle = Geometry.normalizeAngle(angle);
            calculateCachedSinAndCos();
        }
        float velocityDelta = acceleration * seconds;
        float velocityDeltaX = cosAngle * velocityDelta;
        float velocityDeltaY = sinAngle * velocityDelta;
        x = x + velocity * cosAngle * seconds + seconds * velocityDeltaX / 2;
        y = y + velocity * sinAngle * seconds + seconds * velocityDeltaY / 2;
        velocity = velocity + velocityDelta;
    }

    /**
     * Invoke the driver. No changes to body, it only generates commands.
     * @param seconds
     * @return list of commands
     */
    public List<Command> drive(float seconds) {
        if (driver != null) {
            return driver.drive(this, seconds);
        } else {
            return List.of();
        }
    }

    private void calculateCachedSinAndCos() {
        this.cosAngle = Geometry.getX(angle, 1);
        this.sinAngle = Geometry.getY(angle, 1);
    }
}
