package org.isokissa.crowd.sim;

import java.util.Optional;

/**
 * Contains useful functions for transforming between polar and cartesian coordinate systems.
 * By convention, angles are expressed in degrees and lenghts in meters.
 */
public class CollisionDetector {
    public final static float PARALLEL_APPROACHING = 0.1f;

    public static class CollisionInformation {
        public enum Direction {
            APPROACHING_THE_SLOWER,
            HEAD_ON,
            FROM_BACK,
            FROM_RIGHT,
            FROM_LEFT
        }

        public CollisionInformation(PlaneBody otherBody, float secondsToCollision, Direction direction) {
            this.otherBody = otherBody;
            this.secondsToCollision = secondsToCollision;
            this.direction = direction;
        }

        public final PlaneBody otherBody;
        public final float secondsToCollision;
        public final Direction direction;
    }

    /**
     * Calculates in how many seconds will {@code firstBody} collide with {@code otherBody}, assuming
     * that {@code firstBody} and {@code otherBody} continue with their current velocities.
     * @param firstBody
     * @param otherBody
     * @return time to the collision in seconds, no value if bodies will not collide. Zero means
     * that bodies occupy the same position, taking into account the size of bodies,
     * so they are already collided.
     */
    public static Optional<CollisionInformation> calculate(PlaneBody firstBody, PlaneBody otherBody) {
        if (firstBody == otherBody) {
            return Optional.empty();
        }
        Optional<Float> tX = secondsToCollisionPerAxis(
                firstBody.getX(), firstBody.getVelocity() * firstBody.getCosAngle(),
                otherBody.getX(), otherBody.getVelocity() * otherBody.getCosAngle());
        Optional<Float> tY = secondsToCollisionPerAxis(
                firstBody.getY(), firstBody.getVelocity() * firstBody.getSinAngle(),
                otherBody.getY(), otherBody.getVelocity() * otherBody.getSinAngle());
        if (tX.isPresent() && tY.isPresent()) {
            CollisionInformation.Direction direction = calculateDirection(firstBody, otherBody);
            if (tX.get() == 0 || tY.get() == 0) {
                float nonAdjustedSecondsToCollision = Math.max(tX.get(), tY.get());
                return Optional.of(new CollisionInformation(
                        otherBody,
                        collisionTimeWithAdjustmentForPrallel(firstBody, otherBody,
                                                              direction, nonAdjustedSecondsToCollision),
                        direction));
            }
            float timeDifference = Math.abs(tX.get() - tY.get());
            if (timeDifference * Math.max(firstBody.getVelocity(), otherBody.getVelocity()) <= PlaneBody.BODY_DIAMETER) {
                float secondsToCollision = (tX.get() + tY.get()) / 2f;
                return Optional.of(new CollisionInformation(
                        otherBody,
                        collisionTimeWithAdjustmentForPrallel(firstBody, otherBody, direction, secondsToCollision),
                        direction));
            }
        }
        return Optional.empty();
    }

    private static Optional<Float> secondsToCollisionPerAxis(float position1, float velocity1,
                                                             float position2, float velocity2) {
        if (Math.abs(position1 - position2) < PlaneBody.BODY_DIAMETER) {
            return Optional.of(Float.valueOf(0));
        }
        float vxDiff = velocity1 - velocity2;
        if (vxDiff == 0) {
            // equal velocities, not possible to conclude anything
            return Optional.empty();
        } else {
            float t = (position2 - position1) / vxDiff;
            if (t >= 0) {
                return Optional.of(t);
            } else {
                // going further away from each other
                return Optional.empty();
            }
        }
    }

    private static float collisionTimeWithAdjustmentForPrallel(PlaneBody firstBody, PlaneBody otherBody,
                                                               CollisionInformation.Direction direction,
                                                               float collisionTimeBeforeAdjustment) {
        final float oneMeter = 1;
        float angleDifference = Geometry.angleMinDifference(firstBody.getAngle(), otherBody.getAngle());
        boolean isAlmostPrallel = Math.abs(angleDifference) < 20;
        boolean isVeryNear =
                Math.hypot(firstBody.getX() - otherBody.getX(),
                        firstBody.getY() - otherBody.getY()) <= PlaneBody.BODY_DIAMETER + oneMeter;
        boolean isOnTheSide = direction == CollisionInformation.Direction.FROM_LEFT ||
                direction == CollisionInformation.Direction.FROM_RIGHT;
        System.out.println("petar vrlo je blizu " + isVeryNear + isAlmostPrallel + isOnTheSide);
        if (isAlmostPrallel && isVeryNear && isOnTheSide && PARALLEL_APPROACHING < collisionTimeBeforeAdjustment) {
            return PARALLEL_APPROACHING;
        } else {
            return collisionTimeBeforeAdjustment;
        }
    }

    private static CollisionInformation.Direction calculateDirection(PlaneBody b1, PlaneBody b2) {
        if (b2.getVelocity() == 0) {
            return CollisionInformation.Direction.APPROACHING_THE_SLOWER;
        }
        float angleToOther = b1.angleTo(b2.getX(), b2.getY());
        if (150 <= angleToOther && 210 >= angleToOther) {
            return CollisionInformation.Direction.FROM_BACK;
        }
        if ((30 >= angleToOther && 0 <= angleToOther) || (330 <= angleToOther && 360 >= angleToOther)) {
            if (60f > Math.abs(b1.getAngle() - b2.getAngle())) {
                return CollisionInformation.Direction.APPROACHING_THE_SLOWER;
            } else {
                return CollisionInformation.Direction.HEAD_ON;
            }
        }
        if (30 <= angleToOther && 150 >= angleToOther) {
            return CollisionInformation.Direction.FROM_LEFT;
        } else {
            return CollisionInformation.Direction.FROM_RIGHT;
        }
    }
}
