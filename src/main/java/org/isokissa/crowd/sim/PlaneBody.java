package org.isokissa.crowd.sim;

/**
 * Model a body in 2D plane. Contains some essential (about position and orientation)
 * and some useful functions (to calculate relationship to other plane bodies)
 */
public interface PlaneBody {
    enum State {
        INVISIBLE, NORMAL, COLLIDED,
    }

    float BODY_DIAMETER = 3;

    State getState();

    float getX();

    float getY();

    /**
     * Gets the angle in degrees in which the body is oriented in the plane.
     * Zero degrees is along the x-axis.
     * @return angle in degrees values 0 - 360
     */
    float getAngle();

    /**
     * Gets the body's velicity in meters per second.
     * @return body's velocity in m/sec
     */
    float getVelocity();

    default float getDistanceTo(PlaneBody otherBody) {
        return (float)Math.hypot(getX() - otherBody.getX(), getY() - otherBody.getY());
    }

    /**
     * Calculates the minimal angle difference to the given point
     * @param x
     * @param y
     * @return angle to the point, in degrees, how mucht the course of the body has to change
     * in order to get directed towards the body.
     */
    default float angleTo(float x, float y) {
        float absoluteAngleTo = Geometry.angleTo(this.getX(), this.getY(), x, y);
        return Geometry.angleMinDifference(getAngle(), absoluteAngleTo);
    }

    /**
     * @return cosinus of this body's current angle. The idea is to calculate this value
     * only once and use it multiple times.
     */
    float getCosAngle();

    /**
     * @return sinus of this body's current angle. The idea is to calculate this value
     * only once and use it multiple times.
     */
    float getSinAngle();
}
