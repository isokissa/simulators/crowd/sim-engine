package org.isokissa.crowd.sim;

/**
 * Contains useful functions for transforming between polar and cartesian coordinate systems.
 * By convention, angles are expressed in degrees and lenghts in meters.
 */
public class Geometry {

    public static float getX(float angle, float r) {
        return r * (float)Math.cos(Math.toRadians(angle));
    }

    public static float getY(float angle, float r) {
        return r * (float)Math.sin(Math.toRadians(angle));
    }

    public static float angleTo(float x1, float y1, float x2, float y2) {
        double angleDiff = Math.atan2(y2 - y1, x2 - x1);
        return (float)Math.toDegrees(angleDiff);
    }

    /**
     * Calculates numerically the derivate of the square of the distance between two bodies as
     * function of time. It is calculated as (Dist(t + dt) - Dist(t)) / dt
     * @param body1
     * @param body2
     * @return positive number if distance is increasing, negative if the distance is decreasing, zero
     * if disntance is not changing.
     */
    public static float distanceDerivate(Body body1, Body body2) {
        float dt = 1e-3f;
        float xDiffOld = body1.getX() - body2.getX();
        float yDiffOld = body1.getY() - body2.getY();
        float dxDiff = (body1.getCosAngle() * body1.getVelocity() - body2.getCosAngle() * body2.getVelocity()) * dt;
        float dyDiff = (body1.getSinAngle() * body1.getVelocity() - body2.getSinAngle() * body2.getVelocity()) * dt;
        float distOld = xDiffOld * xDiffOld + yDiffOld * yDiffOld;
        float distNew = (xDiffOld + dxDiff) * (xDiffOld + dxDiff) + (yDiffOld + dyDiff) * (yDiffOld + dyDiff);

        float derivate = (distNew - distOld) / dt;
        return derivate;
    }

    /**
     * Calculates the minimal angle difference between two angles.
     * @param a
     * @param b
     * @return how many degrees should rotate angle a to get to angle b (-179, 180]
     */
    public static float angleMinDifference(float a, float b) {
        if (Math.abs(a) > 360) {
            throw new IllegalArgumentException("Angle " + a + " out of range");
        }
        if (Math.abs(b) > 360) {
            throw new IllegalArgumentException("Angle " + b + " out of range");
        }
        float normA = normalizeAngle(a);
        float normB = normalizeAngle(b);
        float diff = normB - normA;
        if (diff < -180) {
            diff = 360 + diff;
        }
        return diff;
    }

    /**
     * @param angle angle [-720, +720]
     * @return normalized angle [0, 360)
     */
    public static float normalizeAngle(float angle) {
        if (angle >= 360) {
            angle -= 360;
        } else if (angle <= -360) {
            angle += 360;
        }
        if (Math.abs(angle) >= 360) {
            angle = normalizeAngle(angle); // cheaper than modulo?
        }
        return angle >= 0 ? angle : 360 + angle;
    }
}
