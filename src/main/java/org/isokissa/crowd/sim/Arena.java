package org.isokissa.crowd.sim;

import org.isokissa.crowd.sim.commands.Command;
import org.isokissa.crowd.sim.lifecycle.Drain;
import org.isokissa.crowd.sim.lifecycle.Spawner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.isokissa.crowd.sim.CollisionDetector.calculate;

public class Arena implements Environment {

    private List<Body> bodies;
    private List<Spawner> spawners;
    private List<Drain> drains;

    public Arena() {
        bodies = new ArrayList<>(200);
        spawners = new ArrayList<>(20);
        drains = new ArrayList<>(20);
    }

    public List<PlaneBody> getAllBodies() {
        return bodies.parallelStream().collect(Collectors.toList());
    }

    public void add(Body body) {
        bodies.add(body);
    }

    public void add(Spawner spawner) {
        spawners.add(spawner);
    }

    public void add(Drain drain) {
        drains.add(drain);
    }

    @Override
    public List<PlaneBody> getBodies(float minX, float maxX, float minY, float maxY) {
        return bodies.parallelStream()
                .filter(b -> b.getX() > minX && b.getX() < maxX && b.getY() > minY && b.getY() < maxY)
                .collect(Collectors.toList());
    }

    public void advance(float seconds) {
        advanceSpawners(seconds);
        advanceDrains();
        List<Command> commands = bodies.parallelStream()
                .flatMap(body -> body.drive(seconds).stream())
                .collect(Collectors.toList());
        commands.parallelStream().forEach(command -> command.execute());
        markCollidedBodies();
    }

    private void advanceSpawners(float seconds) {
        spawners.forEach(s -> {
            Optional<Body> body = s.advance(seconds);
            if (body.isPresent()) {
                bodies.add(body.get());
            }
        });
    }

    private void advanceDrains() {
        drains.forEach(d -> {
            List<PlaneBody> drainedBodies = d.getDrained(this);
            drainedBodies.forEach(b -> bodies.remove(b));
        });
    }

    private void markCollidedBodies() {
        IntStream.range(0, bodies.size() - 1)
                .filter(i -> bodies.get(i).getState() != PlaneBody.State.INVISIBLE)
                .flatMap(i -> IntStream.range(i + 1, bodies.size())
                                .filter(j -> bodies.get(j).getState() != PlaneBody.State.INVISIBLE)
                                .flatMap(j -> {
                                    Optional<CollisionDetector.CollisionInformation> ci = calculate(bodies.get(i), bodies.get(j));
                                    if (ci.isPresent() && ci.get().secondsToCollision == 0f) {
                                        return IntStream.of(i, j);
                                    } else {
                                        return IntStream.empty();
                                    }
                                }))
                .distinct()
                .forEach(i -> bodies.get(i).setState(PlaneBody.State.COLLIDED));
    }
}
