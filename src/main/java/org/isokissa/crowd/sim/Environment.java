package org.isokissa.crowd.sim;

import java.util.List;

public interface Environment {

    List<PlaneBody> getBodies(float minX, float maxX, float minY, float maxY);

}
