package org.isokissa.crowd.sim.commands;

@FunctionalInterface
public interface Command {
    void execute();
}
