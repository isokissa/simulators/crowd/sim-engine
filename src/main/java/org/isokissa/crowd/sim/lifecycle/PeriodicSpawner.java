package org.isokissa.crowd.sim.lifecycle;

import org.isokissa.crowd.sim.Body;
import org.isokissa.crowd.sim.drivers.Driver;
import org.isokissa.crowd.sim.drivers.DriverGenerator;

import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

public class PeriodicSpawner extends Spawner {
    protected float period = 1;
    protected float angle = 0;
    protected int spreading = 0;

    protected float secondsSinceLastTime = 0;

    public PeriodicSpawner(float x, float y, float period, float angle, int spreading, DriverGenerator driverGenerator) {
        super(x, y, driverGenerator);
        this.period = period;
        this.angle = angle;
        this.spreading = spreading;
    }

    /**
     * Spawns a new body if {@code period} seconds has passed since last call to this
     * method. Note that if method is not frequently enough called, it will not be able
     * to spawn enough bodies, because it can spawn at most one body per invocation.
     * @param seconds amount of seconds passed since last call. This time can be used
     *                to determine the right moment when to spawn the body, depending on
     *                each spawners' strategy.
     * @return
     */
    @Override
    public Optional<Body> advance(float seconds) {
        secondsSinceLastTime = secondsSinceLastTime + seconds;
        if (secondsSinceLastTime >= period) {
            secondsSinceLastTime = secondsSinceLastTime - period;
            float angle = this.angle;
            if (spreading > 0) {
                angle = angle + ThreadLocalRandom.current().nextInt(-spreading, +spreading);
            }
            Body body = new Body(x, y, angle);
            Driver driver = driverGenerator.get();
            body.setDriver(driver);
            return Optional.of(body);
        } else {
            return Optional.empty();
        }
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
