package org.isokissa.crowd.sim.lifecycle;

import org.isokissa.crowd.sim.Environment;
import org.isokissa.crowd.sim.PlaneBody;

import java.util.List;

public class Drain {
    private float x;
    private float y;
    private float size;

    public Drain(float x, float y, float size) {
        this.x = x;
        this.y = y;
        this.size = size;
    }

    public List<PlaneBody> getDrained(Environment env) {
        return env.getBodies(x - size, x + size, y - size, y + size);
    }
}
