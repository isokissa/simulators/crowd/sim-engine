package org.isokissa.crowd.sim.lifecycle;

import org.isokissa.crowd.sim.Body;
import org.isokissa.crowd.sim.drivers.DriverGenerator;

import java.util.Optional;

/**
 * Interface for Body Spawners. Subclasses are free to implement their own
 * spawning strategy, and decide what kind of body to spawn, where and at what moment.
 */
public abstract class Spawner {
    protected float x;
    protected float y;
    protected DriverGenerator driverGenerator;

    protected Spawner(float x, float y, DriverGenerator driverGenerator) {
        this.x = x;
        this.y = y;
        this.driverGenerator = driverGenerator;
    }

    /**
     * The method to advance a spawner, and give it a chance to decide whether it will spawn
     * a new body or not. The new body will have initial position same as
     * the spawner's position. This is why it does not make sense to spawn more than one
     * body in one call.
     *
     * Subclasses are supposed to override this method and implement their own meaningful
     * body spawning strategy. The default implementation just creates a new body every time
     * advance is called, regardless of {@code seconds}
     * @param seconds amount of seconds passed since last call. This time can be used
     *                to determine the right moment when to spawn the body, depending on
     *                each spawners' strategy.
     * @return new body with initial position equal to spawner's position, or null, if
     * spawner has decided not to generate a new body.
     */
    public abstract Optional<Body> advance(float seconds);
}
