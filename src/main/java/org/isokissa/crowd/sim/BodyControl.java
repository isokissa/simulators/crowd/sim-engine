package org.isokissa.crowd.sim;

import org.isokissa.crowd.sim.drivers.Driver;

/**
 * Interface to control a Body. Only acceleration and turning
 * speed can be set, pretty much like in real world.
 */
public interface BodyControl {

    /**
     * Advances the body state as if <code>seconds</code> seconds have
     * passed.
     * @param seconds the amount of time passed.
     * @param acceleration acceleration of the body, in the direction of body's angle, in m/sec**2
     * @param steeringSpeed steering speed in degrees per second. Positive steering speed means
 *                      increasing angle in positive direction, which means anti-clockwise.
     */
    void advance(float seconds,
                 float acceleration,
                 float steeringSpeed);

    /**
     * Sets new state.
     * @param newState new state
     */
    void setState(PlaneBody.State newState);

    /**
     * Sets new driver
     * @param driver a new Driver
     */
    void setDriver(Driver driver);
}
