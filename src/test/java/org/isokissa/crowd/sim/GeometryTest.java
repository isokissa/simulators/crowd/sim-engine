package org.isokissa.crowd.sim;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GeometryTest {
    private final float almostZero = 1e-5f;

    @Test
    public void transformOrigin() {
        assertEquals(0, Geometry.getX(45, 0), almostZero);
        assertEquals(0, Geometry.getY(45, 0), almostZero);
    }

    @Test
    public void transformAxisX() {
        assertEquals(10, Geometry.getX(0, 10), almostZero);
        assertEquals(0, Geometry.getY(0, 10), almostZero);
    }

    @Test
    public void transformAxisY() {
        assertEquals(0, Geometry.getX(90, 10), almostZero);
        assertEquals(10, Geometry.getY(90, 10), almostZero);
    }

    @Test
    public void transformAxis3rdQuadrant() {
        final float inRadians = 1.25f * (float)Math.PI;
        assertEquals((float)Math.cos(inRadians) * 10, Geometry.getX(225, 10), almostZero);
        assertEquals((float)Math.sin(inRadians) * 10, Geometry.getY(225, 10), almostZero);
    }

    @Test
    public void angleToPoint2() {
        assertEquals(-45, Geometry.angleTo(10, 10, 20, 0));
        assertEquals(45, Geometry.angleTo(-10, 0, 0, 10));
    }

    @Test
    public void angleMinDifferenceThrowsIfArgumentsAreOutOfRange() {
        angleMinDifferenceVerifyThrowing(361, 0);
        angleMinDifferenceVerifyThrowing(-361, 0);
        angleMinDifferenceVerifyThrowing(0, 361);
        angleMinDifferenceVerifyThrowing(0, -361);
    }

    private void angleMinDifferenceVerifyThrowing(float a, float b) {
        assertThrows(IllegalArgumentException.class, () -> {
            Geometry.angleMinDifference( a, b);
        });
    }

    @Test
    public void angleMinDifferenceWorks() {
        assertEquals(0, Geometry.angleMinDifference(5, 5));
        assertEquals(0, Geometry.angleMinDifference(-5, 355));
        assertEquals(1, Geometry.angleMinDifference(5, 6));
        assertEquals(-1, Geometry.angleMinDifference(6, 5));
        assertEquals(11, Geometry.angleMinDifference(-5, 6));
        assertEquals(11, Geometry.angleMinDifference(355, 6));
        assertEquals(-175, Geometry.angleMinDifference(-5, 180));
        assertEquals(175, Geometry.angleMinDifference(5, -180));
    }

    @Test
    public void normalizeWorks() {
        assertEquals(0, Geometry.normalizeAngle(0));
        assertEquals(0, Geometry.normalizeAngle(360));
        assertEquals(0, Geometry.normalizeAngle(720));
        assertEquals(350, Geometry.normalizeAngle(-10));
        assertEquals(0, Geometry.normalizeAngle(-360));
        assertEquals(0, Geometry.normalizeAngle(-720));
        assertEquals(180, Geometry.normalizeAngle(-180));
        assertEquals(179, Geometry.normalizeAngle(-181));
    }
}