package org.isokissa.crowd.sim;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GeometryDistanceDerivateTest {
    private Body body1;
    private Body body2;

    @Test
    public void zeroForStationaryBodies() {
        body1 = Utils.spyBody(10, 10, 0, 0);
        body2 = Utils.spyBody( 20, 20, 0, 0);
        assertEquals(0, Geometry.distanceDerivate(body1, body2));
    }

    @Test
    public void zeroForBodiesMovingInParallel() {
        setParallelMovingBodies(0);
        assertEquals(0, Geometry.distanceDerivate(body1, body2));
    }

    @Test
    public void positiveForBodiesMovingAlmostInParallelButParting() {
        setParallelMovingBodies(1);
        assertTrue(0 < Geometry.distanceDerivate(body1, body2));
    }

    @Test
    public void negativeForBodiesMovingAlmostInParallelButGettingCloser() {
        setParallelMovingBodies(-1);
        assertTrue(0 > Geometry.distanceDerivate(body1, body2));
    }

    private void setParallelMovingBodies(float angleDifference) {
        float sameAngle = 30;
        float sameVelocity = 100;
        body1 = Utils.spyBody(10, 10, sameAngle + angleDifference, sameVelocity);
        body2 = Utils.spyBody(30, 20, sameAngle, sameVelocity);
    }

    @Test
    public void negativeWhenOneStationaryAndOtherApproaching() {
        body1 = Utils.spyBody(100, 10, 0, 0);
        body2 = Utils.spyBody(5, 50, -20, 10);
        assertTrue(0 > Geometry.distanceDerivate(body1, body2));
        assertTrue(0 > Geometry.distanceDerivate(body2, body1));
    }

    @Test
    public void positiveWhenOneStationaryAndOtherParting() {
        body1 = Utils.spyBody(100, 10, 0, 0);
        body2 = Utils.spyBody(5, 50, 90, 10);
        assertTrue(0 < Geometry.distanceDerivate(body1, body2));
        assertTrue(0 < Geometry.distanceDerivate(body2, body1));
    }
}