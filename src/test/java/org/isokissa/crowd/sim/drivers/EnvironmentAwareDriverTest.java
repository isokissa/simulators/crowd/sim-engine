package org.isokissa.crowd.sim.drivers;

import org.isokissa.crowd.sim.Environment;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class EnvironmentAwareDriverTest {
    private EnvironmentAwareDriver driver;

    private final Environment envMock = mock(Environment.class);

    @Test
    public void constructs() {
        driver = new EnvironmentAwareDriver(envMock);
    }
}