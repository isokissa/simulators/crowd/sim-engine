package org.isokissa.crowd.sim.drivers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class DriverGeneratorTest {

    @Test
    public void driverGeneratorIsDefined() {
        Driver mockDriver = mock(Driver.class);
        DriverGenerator dg = () -> mockDriver;
        assertTrue(mockDriver == dg.get());
    }
}