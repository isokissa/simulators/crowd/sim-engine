package org.isokissa.crowd.sim.drivers;

import org.isokissa.crowd.sim.PlaneBody;
import org.isokissa.crowd.sim.Utils;
import org.isokissa.crowd.sim.commands.Command;
import org.isokissa.crowd.sim.Body;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class BlindDriverTest {
    private final float x = 10;
    private final float y = 20;
    private final float angle = 30;
    private final float velocity = 10f;
    private final float seconds = 2f;

    private Body body;
    private BlindDriver driver;

    @BeforeEach
    public void setup() {
        driver = new BlindDriver(velocity);
    }

    @Test
    public void accelerateUntilReachesTheSpeed() {
        body = Utils.spyBody(x, y, angle, 0);
        List<Command> commands = driver.drive(body, seconds);
        assertEquals(1, commands.size());
        commands.forEach(c -> c.execute());
        verify(body, times(1)).advance(seconds, BlindDriver.MAX_ACCELERATION, 0);
    }

    @Test
    public void whenFirstTimeReachTheNormalVelocityChangeBodyStateToNormal() {
        body = Utils.spyBody(x, y, angle, velocity + 1);
        List<Command> commands = driver.drive(body, seconds);
        commands.forEach(c -> c.execute());
        verify(body, times(1)).setState(PlaneBody.State.NORMAL);
    }
}