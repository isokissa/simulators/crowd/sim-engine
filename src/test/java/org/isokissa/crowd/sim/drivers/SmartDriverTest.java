package org.isokissa.crowd.sim.drivers;

import org.isokissa.crowd.sim.Body;
import org.isokissa.crowd.sim.Environment;
import org.isokissa.crowd.sim.PlaneBody;
import org.isokissa.crowd.sim.Utils;
import org.isokissa.crowd.sim.commands.Command;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class SmartDriverTest {
    private final Environment env = mock(Environment.class);
    private final float destX = 100;
    private final float destY = 500;
    private final float cruisingVelocity = 30;
    private SmartDriver driver;

    @BeforeEach
    public void setUp() {
        reset(env);
        driver = new SmartDriver(env, destX, destY, cruisingVelocity);
        driver.setState(SmartDriver.State.NORMAL);
    }

    @Test
    public void initiallyTheDriverIsInInitialState() {
        SmartDriver freshDriver = new SmartDriver(env, destX, destY, cruisingVelocity);

        assertEquals(SmartDriver.State.INITIAL, freshDriver.getState());
    }

    @Test
    public void whenInitiallyReachCruisingSpeedGoesToNormalState() {
        float velocityDifference = 5;
        float seconds = 3;
        SmartDriver freshDriver = new SmartDriver(env, destX, destY, cruisingVelocity);
        Body body = Utils.spyBody(0, 0, 0, cruisingVelocity - velocityDifference);

        List<Command> commands = freshDriver.drive(body, seconds);

        assertEquals(SmartDriver.State.NORMAL, freshDriver.getState());
        assertExpectedAdvance(commands, body, seconds, velocityDifference / seconds, 0);
        verify(body, times(1)).setState(PlaneBody.State.NORMAL);
    }

    @Test
    public void whenInitiallyNotEnoughTimeToReachCruisingSpeedJustKeepAcceleratingWithNoStateChanges() {
        float velocityDifference = 20;
        float seconds = 0.2f;
        SmartDriver freshDriver = new SmartDriver(env, destX, destY, cruisingVelocity);
        Body body = Utils.spyBody(0, 0, 0, cruisingVelocity - velocityDifference);

        List<Command> commands = freshDriver.drive(body, seconds);

        assertEquals(SmartDriver.State.INITIAL, freshDriver.getState());
        assertExpectedAdvance(commands, body, seconds, freshDriver.MAX_ACCELERATION, 0);
        verify(body, times(0)).setState(any());
    }

    @Test
    public void ifNoObstaclesAndAlreadySteeredWillNotDoAnythingOnlyAdvance() {
        Body wellDirectedBody = Utils.spyBody(destX - 100, destY - 100, 45, cruisingVelocity);

        List<Command> commands = driver.drive(wellDirectedBody, 1);

        assertExpectedAdvance(commands, wellDirectedBody,1, 0, 0);
    }

    @Test
    public void ifNoObstaclesAndNotEnoughSpeedAccelerateWithHalfMaxAccelerationBecauseTimeIsDouble() {
        Body wellDirectedButSlowBody = Utils.spyBody(destX - 100, destY - 100, 45, 0);
        float timeNeededToAccelerateFully = cruisingVelocity / SmartDriver.MAX_ACCELERATION;

        List<Command> commands = driver.drive(wellDirectedButSlowBody, timeNeededToAccelerateFully * 2);

        assertExpectedAdvance(commands, wellDirectedButSlowBody,
                timeNeededToAccelerateFully * 2, SmartDriver.MAX_ACCELERATION / 2, 0);
    }

    @Test
    public void ifNoObstaclesAndNotEnoughSpeedAccelerateAsMuchAsPossible() {
        Body wellDirectedButSlowBody = Utils.spyBody(destX - 100, destY - 100, 45, 0);
        float timeNeededToAccelerateFully = cruisingVelocity / SmartDriver.MAX_ACCELERATION;

        List<Command> commands = driver.drive(wellDirectedButSlowBody, timeNeededToAccelerateFully / 2);

        assertExpectedAdvance(commands, wellDirectedButSlowBody,
                timeNeededToAccelerateFully / 2, SmartDriver.MAX_ACCELERATION, 0);
    }

    @Test
    public void withoutOthersWillSteerTowardsDestinationFullyIfStepIsBigEnough() {
        final float angleToDestination = 45;
        Body body = Utils.spyBody(destX, destY - 400, angleToDestination, cruisingVelocity);
        final float timeNeededToSteerCompletely = angleToDestination / SmartDriver.MAX_STEERING_SPEED;
        final float seconds = timeNeededToSteerCompletely + 1;

        List<Command> commands = driver.drive(body, seconds);

        assertExpectedAdvance(commands, body, seconds, 0, angleToDestination / seconds);
    }

    @Test
    public void withoutOthersWillSteerTowardsDestinationPartiallyWhenStepIsHalfEnough() {
        final float angleToDestination = 45;
        Body body = Utils.spyBody(destX, destY - 400, angleToDestination, cruisingVelocity);
        final float timeNeededToSteerCompletely = angleToDestination / SmartDriver.MAX_STEERING_SPEED;

        List<Command> commands = driver.drive(body, timeNeededToSteerCompletely / 2f);

        assertExpectedAdvance(commands, body,
                timeNeededToSteerCompletely / 2f, 0, SmartDriver.MAX_STEERING_SPEED);
    }

    @Test
    public void withIrrelevantBodiesSteersTowardsDestinationTheSameWay1() {
        addIrrelevantNearBody(destX, destY - 400);
        withoutOthersWillSteerTowardsDestinationFullyIfStepIsBigEnough();
    }

    @Test
    public void withIrrelevantBodiesSteersTowardsDestinationTheSameWay2() {
        addIrrelevantNearBody(destX, destY - 400);
        withoutOthersWillSteerTowardsDestinationPartiallyWhenStepIsHalfEnough();
    }

    private void addIrrelevantNearBody(float x, float y) {
        Body body = Utils.spyBody(x - SmartDriver.SCAN_SIZE / 2, y - SmartDriver.SCAN_SIZE / 2, -10, 5);
        body.setState(PlaneBody.State.NORMAL);
        when(env.getBodies(anyFloat(), anyFloat(), anyFloat(), anyFloat()))
                .thenReturn(List.of(body));
    }

    @Test
    public void toOvertakeTurnLeftAndTheSlowerOneTurnsSlightlyToTheRight() {
        float seconds = 0.01f;
        Body slowBody = Utils.spyBody(destX, destY - 400, 90, 20);
        Body fastBody = Utils.spyBody(destX, destY - 415, 90, 30);
        when(env.getBodies(anyFloat(), anyFloat(), anyFloat(), anyFloat()))
                .thenReturn(List.of(slowBody, fastBody));

        List<Command> commandsForFaster = driver.drive(fastBody, seconds);
        List<Command> commandsForSlower = driver.drive(slowBody, seconds);

        assertExpectedAdvance(commandsForFaster, fastBody, seconds, 0, SmartDriver.OVERTAKING_STEERING);
        assertExpectedAdvance(commandsForSlower, slowBody, seconds, 0, SmartDriver.GIVE_WAY);
    }

    @Test
    public void whenApproachingFromSideTheLeftWillTurnTowardsTheOtherAndTheOtherInSameDirection() {
        float seconds = 0.01f;
        Body leftBody = Utils.spyBody(destX, destY + 10, -90, 20);
        Body rightBody = Utils.spyBody(destX - 10, destY, 0, 20);
        when(env.getBodies(anyFloat(), anyFloat(), anyFloat(), anyFloat()))
                .thenReturn(List.of(leftBody, rightBody));

        List<Command> commandsForLeft = driver.drive(leftBody, seconds);
        List<Command> commandsForRight = driver.drive(rightBody, seconds);

        assertExpectedAdvance(commandsForLeft, leftBody, seconds, 0, - SmartDriver.OVERTAKING_STEERING);
        assertExpectedAdvance(commandsForRight, rightBody, seconds, 0, - SmartDriver.OVERTAKING_STEERING);
    }

    private void assertExpectedAdvance(List<Command> commands, Body body,
                                       float seconds, float acceleration, float steering) {
        commands.stream().forEach(c -> c.execute());
        verify(body, times(1)).
                advance(seconds, acceleration, steering);
    }

}