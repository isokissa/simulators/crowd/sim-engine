/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package org.isokissa.crowd.sim.lifecycle;

import org.isokissa.crowd.sim.Body;
import org.isokissa.crowd.sim.drivers.BlindDriver;
import org.isokissa.crowd.sim.drivers.DriverGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class PeriodicSpawnerTest {
    private final float x = 5;
    private final float y = 105;
    private final float period = 3f;
    private final float seconds = 2f;

    private PeriodicSpawner periodicSpawner;

    private DriverGenerator driverGenerator = () -> new BlindDriver(20);

    @BeforeEach
    public void setup() {
        this.periodicSpawner = new PeriodicSpawner(x, y, period,
                0, 0, driverGenerator);
    }

    @Test
    public void makeBodyGeneratesBodyInSpawnersLocation() {
        Optional<Body> body = periodicSpawner.advance(period);
        assertNotNull(body.get());
        Body b = body.get();
        assertEquals(x, b.getX());
        assertEquals(y, b.getY());
    }

    @Test
    public void makeOneBodyPerEveryThreeSeconds() {
        assertTrue(periodicSpawner.advance(seconds).isEmpty());
        assertTrue(periodicSpawner.advance(seconds).isPresent());
        assertTrue(periodicSpawner.advance(seconds).isPresent());
        assertTrue(periodicSpawner.advance(seconds).isEmpty());
    }
}
