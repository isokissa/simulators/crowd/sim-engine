package org.isokissa.crowd.sim;

import org.isokissa.crowd.sim.drivers.Driver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

class BodyControlTest {
    private final float x = 10;
    private final float y = 20;
    private final float angle = 30;
    private Body body;
    private BodyControl bodyControl;
    private Environment envMock = mock(Environment.class);
    private Driver driverMock = mock(Driver.class);

    @BeforeEach
    public void setup() {
        envMock = mock(Environment.class);
        driverMock = mock(Driver.class);
        body = new Body(x, y, angle);
        bodyControl = body;
    }

    @Test
    public void invokesTheDriver() {
        bodyControl.setDriver(driverMock);
        final float seconds = 2;
        body.drive(seconds);
        verify(driverMock, times(1)).drive(body, seconds);
    }

    @Test
    public void advanceWithNoVelocityWillReturnTheSameBody() {
        final float seconds = 0.1f;
        final float acceleration = 0; 
        final float steeringSpeed = 0; 

        bodyControl.advance(seconds, acceleration, steeringSpeed);

        assertEquals(this.x, ((PlaneBody) body).getX());
        assertEquals(this.y, ((PlaneBody) body).getY());
        assertEquals(this.angle, ((PlaneBody) body).getAngle());
        assertEquals(0f, ((PlaneBody) body).getVelocity());
    }

    @Test
    public void bodyWithAccelerationWillGainSpeed() {
        final float acceleration = 5f;
        final float steeringSpeed = 0; 
        final float seconds = 0.1f;
        bodyControl.advance(seconds, acceleration, steeringSpeed);
        final float expectedVelocity = acceleration * seconds;

        assertEquals(expectedVelocity, body.getVelocity());

        bodyControl.advance(seconds * 3, acceleration, steeringSpeed);

        assertEquals(expectedVelocity * 4, body.getVelocity());
    }

    @Test
    public void advanceBodyWithVelocityWillChangeItsPositionInBothCoordinates() {
        final float acceleration = 2f;
        final float steeringSpeed = 0; 
        final float seconds = 5f;
        final float expectedVelocity = acceleration * seconds;
        final float expectedDistance = expectedVelocity * seconds / 2;
        final float expectedX = Geometry.getX(angle, expectedDistance) + x;
        final float expectedY = Geometry.getY(angle, expectedDistance) + y;

        body.advance(seconds, acceleration, steeringSpeed);

        assertEquals(expectedVelocity, body.getVelocity());
        assertEquals(expectedX, body.getX());
        assertEquals(expectedY, body.getY());
    }

    @Test
    public void advanceBodysPositionAtAcceleratedRate() {
        final float seconds = 2;
        final float acceleration = 10;
        final float steeringSpeed = 0; 
        Body body = new Body(0, 0, 0);
        body.advance(seconds, acceleration, steeringSpeed);

        assertEquals(seconds * seconds * acceleration / 2, body.getX());

        body.advance(seconds, acceleration, steeringSpeed);

        assertEquals( (seconds * 2) * (seconds * 2) * acceleration / 2, body.getX());
    }

    @Test
    public void advanceWithSteeringSpeedWillAdvanceTheAngle() {
        final float seconds = 3;
        final float acceleration = 0; 
        final float steeringSpeed = 2;
        bodyControl.advance(seconds, acceleration, steeringSpeed);

        assertEquals(angle + steeringSpeed * seconds, body.getAngle());
    }

    @Test
    public void advanceWithSteeringWillNormalizeTheAngle() {
        verifyAdvanceSteering(0, 10, 10);
        verifyAdvanceSteering(5, -10, 355);
        verifyAdvanceSteering(355, 10, 5);
        verifyAdvanceSteering(355, 180, 175);
        verifyAdvanceSteering(355, 355, 350);
    }

    private void verifyAdvanceSteering(float initialAngle, float angleToAdvance, float expectedAngle) {
        final float oneSecond = 1;
        final float noAcceleration = 0;
        Body body = new Body(0, 0, initialAngle);
        body.advance(oneSecond, noAcceleration, angleToAdvance);
        assertEquals(expectedAngle, body.getAngle());
    }

    @Test
    public void simultaneousSteeringAndAccelerating() {
        final float acceleration = 3f;
        final float seconds = 4f;
        final float steeringSpeed = 10f;

        bodyControl.advance(seconds, acceleration, steeringSpeed);
        bodyControl.advance(seconds, acceleration, steeringSpeed);

        assertEquals(angle + 2 * seconds * steeringSpeed, body.getAngle());
        assertEquals(2 * acceleration * seconds, body.getVelocity());
    }

    @Test
    public void willStopAndNotReactToCommandsIfCollided() {
        body.setState(PlaneBody.State.COLLIDED);

        bodyControl.advance(1, 10, 10);

        assertEquals(x, body.getX(), "Will not move");
        assertEquals(y, body.getY(), "Will not move");
        assertEquals(0, body.getVelocity());
    }
}