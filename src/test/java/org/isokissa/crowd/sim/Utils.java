package org.isokissa.crowd.sim;

import org.mockito.Mockito;

import static org.mockito.Mockito.*;

public class Utils {

    public static Body spyBody(float x, float y, float angle, float velocity) {
        Body bodySpy = Mockito.spy(new Body(x, y, angle));
        doReturn(velocity).when(bodySpy).getVelocity();
        return bodySpy;
    }
}
