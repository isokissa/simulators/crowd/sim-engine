package org.isokissa.crowd.sim;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlaneBodyTest {
    private final float x = 10;
    private final float y = 20;
    private final float angleDegrees = 45;
    private PlaneBody body;
    private Body body1;
    private Body body2;

    @BeforeEach
    public void setup() {
        body = new Body(x, y, angleDegrees);
    }

    @Test
    public void bodyIsCorrectlyInitialized() {
        assertEquals(x, body.getX());
        assertEquals(y, body.getY());
        assertEquals(angleDegrees, body.getAngle());
        assertEquals(0f, body.getVelocity());
    }

    @Test
    public void angleIsCorrectlyNormalized() {
        verifyNormalizedAngle(0, 0);
        verifyNormalizedAngle(1, 1);
        verifyNormalizedAngle(-1, 359);
        verifyNormalizedAngle(180, 180);
        verifyNormalizedAngle(-180, 180);
        verifyNormalizedAngle(-181, 179);
        verifyNormalizedAngle(-359, 1);
    }

    private void verifyNormalizedAngle(float givenAngle, float expectedNormalizedAngle) {
        Body body = new Body(0, 0, givenAngle);
        assertEquals(expectedNormalizedAngle, body.getAngle());
    }

    @Test
    public void angleInDegrees() {
        assertEquals(body.getAngle(), 45f, 1e-5);
    }

    @Test
    public void calculateDistance() {
        final float x = 20;
        final float y = 10;
        final float expectedDistance = (float)Math.hypot(x - this.x, y - this.y);
        final PlaneBody otherBody = new Body(x, y);

        assertEquals(expectedDistance, body.getDistanceTo(otherBody));
    }

    @Test
    public void calculateRelativeAngle() {
        body1 = new Body(10, 10, 60);
        body2 = new Body(10, 20, 0);

        assertEquals(30f, body1.angleTo(body2.getX(), body2.getY()));
    }

    @Test
    public void distanceToOtherBody() {
        body1 = new Body(10, 10);
        body2 = new Body(20, 20);

        final float expectedDistance = (float)Math.sqrt(200);
        assertEquals(expectedDistance, body1.getDistanceTo(body2));
        assertEquals(expectedDistance, body2.getDistanceTo(body1));
    }
}