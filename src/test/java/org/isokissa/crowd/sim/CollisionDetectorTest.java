package org.isokissa.crowd.sim;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.isokissa.crowd.sim.CollisionDetector.*;
import static org.isokissa.crowd.sim.CollisionDetector.CollisionInformation.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CollisionDetectorTest {
    private Body body1;
    private Body body2;
    private Optional<CollisionInformation> collisionInfo;
    private Optional<CollisionInformation> collisionInfoOtherDirection;

    @Test
    public void collideImmediatelyIfBodiesAreOnSameLocation() {
        body1 = Utils.spyBody(100, 100, 42, 24);
        body2 = Utils.spyBody(100, 100, 24, 42);

        verifyCollisionTime(0);
    }

    @Test
    public void notCollidingWithItself() {
        body1 = Utils.spyBody(0, 0, 10, 30);
        body2 = body1;

        verifyNoCollision();
    }

    @Test
    public void whenCollidingTheInfoContainsReferenceToOtherBody() {
        body1 = Utils.spyBody(100, 100, 42, 24);
        body2 = Utils.spyBody(100, 100, 24, 42);

        calculateInBothDirections();

        assertTrue(collisionInfo.get().otherBody == body2);
        assertTrue(collisionInfoOtherDirection.get().otherBody == body1);
    }

    @Test
    public void collideImmediatelyIfBodiesAreVeryNear() {
        body1 = Utils.spyBody(100, 101, 42, 24);
        body2 = Utils.spyBody(100, 100, 24, 42);

        verifyCollisionTime(0);
    }

    @Test
    public void collideNeverIfBothBodiesDontMove() {
        body1 = Utils.spyBody(10, 5, 42, 0);
        body2 = Utils.spyBody(0, 0, 24, 0);

        verifyNoCollision();
    }

    @Test
    public void collideNeverIfMovingParallel() {
        body1 = Utils.spyBody(10, 20, 30, 5);
        body2 = Utils.spyBody(5, 5, 30, 5);

        verifyNoCollision();
    }

    @Test
    public void collideSoonIfAlmostParallelVeryCloseNextToEachOther() {
        body1 = Utils.spyBody(10, 20.001f + PlaneBody.BODY_DIAMETER, 0, 20);
        body2 = Utils.spyBody(10, 20, 1, 20);

        verifyCollision(PARALLEL_APPROACHING, Direction.FROM_RIGHT, Direction.FROM_LEFT);
    }

    @Test
    public void collideWithStationaryBodyOnX() {
        body1 = Utils.spyBody(0, 0, 0, 5);
        body2 = Utils.spyBody(10, 0, 0, 0);

        verifyCollision(2, Direction.APPROACHING_THE_SLOWER, Direction.FROM_BACK);
    }

    @Test
    public void collideWithStationaryBodyNearX() {
        body1 = Utils.spyBody(0, 1, 0, 5);
        body2 = Utils.spyBody(10, 0, 0, 0);

        verifyCollision(2, Direction.APPROACHING_THE_SLOWER, Direction.FROM_BACK);
    }

    @Test
    public void crossingBodies() {
        body1 = Utils.spyBody(10, 0, -180, 5);
        body2 = Utils.spyBody(0, 10, -90, 5);

        verifyCollision(2, Direction.FROM_RIGHT, Direction.FROM_LEFT);
    }

    @Test
    public void crossingNearEnoughBodies() {
        body1 = Utils.spyBody(10 - PlaneBody.BODY_DIAMETER/3f, 0, -180, 5);
        body2 = Utils.spyBody(0, 10, -90, 5);

        verifyCollision(1.9f, Direction.FROM_RIGHT, Direction.FROM_LEFT);
    }

    @Test
    public void collideNeverWhenGoingDifferentDirectionsWithStationaryBodyOnX() {
        body1 = Utils.spyBody(0, 0, 0, -5);
        body2 = Utils.spyBody(10, 0, 0, 0);

        verifyNoCollision();
    }

    @Test
    public void collideWithSlowerBodyOnX() {
        body1 = Utils.spyBody(0, 0, 0, 4);
        body2 = Utils.spyBody(10, 0, 0, 3);

        verifyCollision(10, Direction.APPROACHING_THE_SLOWER, Direction.FROM_BACK);
    }

    @Test
    public void collideWithStationaryBodyOnY() {
        body1 = Utils.spyBody(0, 0, 90, 5);
        body2 = Utils.spyBody(0, 10, 0, 0);

        verifyCollision(2, Direction.APPROACHING_THE_SLOWER, Direction.FROM_RIGHT);
    }

    @Test
    public void collideWithStationaryBodyNearY() {
        body1 = Utils.spyBody(1, 0, 90, 5);
        body2 = Utils.spyBody(0, 10, 0, 0);

        verifyCollision(2, Direction.APPROACHING_THE_SLOWER, Direction.FROM_RIGHT);
    }

    @Test
    public void collideNeverWhenGoingDifferentDirectionsWithStationaryBodyOnY() {
        body1 = Utils.spyBody(0, 0, 0, -5);
        body2 = Utils.spyBody(0, 10, 0, 0);

        verifyNoCollision();
    }

    @Test
    public void collideWithSlowerBodyOnY() {
        body1 = Utils.spyBody(0, 0, 90, 4);
        body2 = Utils.spyBody(0, 10, 90, 3);

        verifyCollision(10, Direction.APPROACHING_THE_SLOWER, Direction.FROM_BACK);
    }

    @Test
    public void collideDirectlyDiagonallyToStationary() {
        body1 = Utils.spyBody(0, 0, 45, 5);
        body2 = Utils.spyBody(10, 10, 225, 0);

        verifyCollision((float)Math.sqrt(8), Direction.APPROACHING_THE_SLOWER, Direction.HEAD_ON);
    }

    @Test
    public void collideDirectlyDiagonallyHeadon() {
        body1 = Utils.spyBody(0, 0, 45, 3);
        body2 = Utils.spyBody(10, 10, 225, 2);

        verifyCollision((float)Math.sqrt(8), Direction.HEAD_ON, Direction.HEAD_ON);
    }

    private void calculateInBothDirections() {
        this.collisionInfo = calculate(body1, body2);
        this.collisionInfoOtherDirection = calculate(body2, body1);
    }

    private void verifyNoCollision() {
        calculateInBothDirections();
        assertTrue(collisionInfo.isEmpty() && collisionInfoOtherDirection.isEmpty());
    }

    private void verifyCollisionTime(float collisionTime) {
        calculateInBothDirections();

        assertTrue(collisionInfo.isPresent() && collisionInfoOtherDirection.isPresent());
        assertEquals(collisionTime, collisionInfo.get().secondsToCollision);
        assertEquals(collisionTime, collisionInfoOtherDirection.get().secondsToCollision);
    }

    private void verifyCollision(float collisionTime,
                                 CollisionInformation.Direction directionForFirst,
                                 CollisionInformation.Direction directionForSecond) {
        verifyCollisionTime(collisionTime);
        assertEquals(directionForFirst, collisionInfo.get().direction);
        assertEquals(directionForSecond, collisionInfoOtherDirection.get().direction);
    };
}